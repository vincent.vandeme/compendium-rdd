# Compendiums Foundry pour Rêve de Dragon

For [details in English](#in-english)

Ce module contient des aides de jeu pour différents scénarios de Rêve de Dragon.

Son but est de faciliter la préparation des scénarios, il ne remplace pas le scénario.

Les cartes sont développées avec des ressources provenant de différents artistes, et sont proposées gratuitement.

Les illustrations sont de moi, donc utilisables avec mon accord pour vos parties. S'il n'y a pas d'illustrations, ce sera à vous de les ajouter.

## Les scénarios couverts
* A l'Heure du Vaisseau
* Le trésor de Black Squirrel
* L'Oeuf de Psiluma
* La balade de Lorinel

# Mentions Légales
## Auteur et marque déposée

Rêve de Dragon est un jeu de Denis Gerfaud, et une marque déposée par [Scriptarium](https://scriptarium.fr/fr/reve-de-dragon/)

## Contributeurs

Sur le [Discord Foundry FR](https://discord.gg/pPSDNJk)

- Mainteneur/Développeur/Illustrations: VincentVk
- Cartes: VincentVk, LeRatierBretonien

## Illustrations

Les dessins sont de moi, vous pouvez les utiliser pour vos parties. Pour d'autres utilisation, n'hésitez pas à me demander. [D'autres illustrations dans sont sur mon blog](https://vincent-vk.blogspot.com/)

## Cartes et ressources graphiques

Les cartes sont créées avec des ressources graphiques pouvant provenir de:
* [Gogot's assets](https://www.patreon.com/m/7755652/posts)
* [Tom Cartos assets](https://www.patreon.com/tomcartos)
* [Apprentice of Aule](https://cartographyassets.com/asset-tag/aoa/)
* [Forgotten Adventures](https://www.forgotten-adventures.net/) - [Patreon](https://www.patreon.com/forgottenadventures/)

# In English

* Rêve de dragon is a french Role Playing Game, by Denis Gerfaud, trademark [Scriptarium](https://scriptarium.fr/fr/reve-de-dragon/)
* Drawings are my own, for use in your roleplaying games. For different usage, please contact me (VincentVk#9373 on Discord). Shameless self advertising, [other drawings are also on my blog](https://vincent-vk.blogspot.com/)
* maps are based on assets from 
  * [Gogot's assets](https://www.patreon.com/m/7755652/posts)
  * [Tom Cartos assets](https://www.patreon.com/tomcartos)
  * [Apprentice of Aule](https://cartographyassets.com/asset-tag/aoa/)
  * [Forgotten Adventures](https://www.forgotten-adventures.net/) - [Patreon](https://www.patreon.com/forgottenadventures/)
